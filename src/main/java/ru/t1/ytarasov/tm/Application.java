package ru.t1.ytarasov.tm;

import ru.t1.ytarasov.tm.api.ICommandRepository;
import ru.t1.ytarasov.tm.constant.ArgumentConst;
import ru.t1.ytarasov.tm.constant.TerminalConst;
import ru.t1.ytarasov.tm.repository.CommandRepository;
import ru.t1.ytarasov.tm.util.FormatUtil;
import ru.t1.ytarasov.tm.model.Command;

import java.util.Scanner;


public class Application {

    public final static ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        runArguments(args);
    }

    private static void runArguments(String[] args) {
        if (args == null || args.length == 0) {
            runCommands();
            return;
        }
        final String arg = args[0];
        runArgument(arg);
    }

    private static void runArgument(final String arg) {
        displayWelcome();
        switch (arg) {
            case ArgumentConst.ABOUT:
                displayAbout();
                break;
            case ArgumentConst.VERSION:
                displayVersion();
                break;
            case ArgumentConst.HELP:
                displayHelp();
                break;
            case ArgumentConst.INFO:
                displayInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                displayArguments();
                break;
            case ArgumentConst.COMMANDS:
                displayCommands();
                break;
            default:
                displayError();
        }
    }

    private static void runCommands() {
        displayWelcome();
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (true) {
            command = scanner.nextLine();
            runCommand(command);
        }
    }

    private static void runCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                displayAbout();
                break;
            case TerminalConst.VERSION:
                displayVersion();
                break;
            case TerminalConst.HELP:
                displayHelp();
                break;
            case TerminalConst.INFO:
                displayInfo();
                break;
            case TerminalConst.ARGUMENTS:
                displayArguments();
                break;
            case TerminalConst.COMMANDS:
                displayCommands();
                break;
            case TerminalConst.EXIT:
                exitApplication();
            default:
                displayError();
        }
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (Command command : commands) System.out.println(command);
        System.out.println();
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.1");
        System.out.println();
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Yuriy Tarasov");
        System.out.println("ytarasov@t1-consulting.ru");
        System.out.println();
    }

    private static void displayArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
        System.out.println();
    }

    private static void displayCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
        System.out.println();
    }

    private static void displayError() {
        System.out.println("Error. Type help to show availible commands");
        System.out.println();
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO THE TASK MANAGER **");
    }

    private static void exitApplication() {
        System.exit(0);
    }

    private static void displayInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = FormatUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
        System.out.println();
    }

}
